from django.forms import ModelForm
from django import forms
from .models import Student, Participation, Project, Log


class StudentLoginForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'inputPassword', 'type': 'password'}))

    class Meta:
        model = Student
        fields = ['email', 'password']
        # gives standard input fields some css classes:
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control', 'id': 'inputEmail'}),
        }


class AddStudentToProjectForm(ModelForm):

    class Meta:
        model = Participation
        fields = ['student', 'project']
        widgets = {'project': forms.HiddenInput}

    # exclude students who are already in the project:
    def __init__(self, slug=None, *args, **kwargs):
        super(AddStudentToProjectForm, self).__init__(*args, **kwargs)
        project_id = Project.objects.get(title=slug).id
        participation_list = []
        for item in Participation.objects.filter(project_id=project_id):
            student_id = item.student_id
            participation_list.append(student_id)
        self.fields['student'].queryset = Student.objects.exclude(id__in=participation_list).exclude(is_CEO=True).exclude(is_superuser=True)


class StudentNewLogForm(ModelForm):
    hours = forms.DurationField(required=False, widget=forms.HiddenInput)

    class Meta:
        model = Log
        fields = ['student', 'project', 'date', 'start_time', 'end_time', 'description', 'hours']
        widgets = {'project': forms.HiddenInput, 'student': forms.HiddenInput}
