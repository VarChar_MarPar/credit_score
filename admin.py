from django.contrib import admin

# Register your models here.

from .models import Project, Student, Log, Participation

admin.site.register(Project)
admin.site.register(Student)
admin.site.register(Log)
admin.site.register(Participation)