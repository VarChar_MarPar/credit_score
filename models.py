from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.forms import User
from django.contrib.auth.models import AbstractUser
import datetime


# Abstract user extends User. All the fields such as id, email, first_name are set by User class.
class Student(AbstractUser):
    student_number = models.IntegerField(default='12345')
    is_CEO = models.BooleanField(default=False)
    group = models.CharField(max_length=255, default='12345')
    password = models.CharField(max_length=255, default='firma2016')
    projects = models.ManyToManyField('Project', through='Participation')
    remainder = models.FloatField(null=True)
    last_checkout_month = models.IntegerField(null=True) # fix the month
    position = models.CharField(max_length=255, default='Employee')
    taskENG = models.TextField(blank=True)
    taskFIN = models.TextField(blank=True)

    class Meta:
        db_table = 'students'

    def __unicode__(self):
        return self.first_name + " " + self.last_name


class Project(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    students = models.ManyToManyField(Student, through='Participation')
    funding = models.IntegerField(default=0)
    start_date = models.DateField(default=datetime.date.today, blank=True)
    end_date = models.DateField(default=datetime.date.today, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'projects'


class Log(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    hours = models.DurationField()
    status = models.CharField(max_length=15, default="pending")
    description = models.CharField(max_length=100, default=None)

    class Meta:
        db_table = 'logs'


# a model to connect student and project
class Participation(models.Model):
    # in DB names are student_id/project_id
    student = models.ForeignKey(Student, verbose_name='Choose student to add')
    project = models.ForeignKey(Project)

    class Meta:
        db_table = 'participation'
