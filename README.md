# README #

### What is this repository for? ###

* This is a repo for Django project's app credits_score. 
* The project's aim is to create an environment for credits calculation and simplify reporting to the Student Office.

### How do I get set up? ###

* Clone files from this repo to your working_hours Django project
* Adjust settings.py:
    - Add 'credit_score' to INSTALLED_APPS 
* You can push and pull changes to the folder 

### Who do I talk to? ###

Repo owner: Maria Parshina