from django.shortcuts import render
from django.views.generic import ListView, DetailView, FormView, View, TemplateView, DeleteView, UpdateView, RedirectView
from django.views.generic.edit import CreateView, BaseUpdateView
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, reverse
from django.contrib.auth.mixins import UserPassesTestMixin
from django.urls import reverse_lazy
from datetime import datetime, timedelta
from django.db.models import Sum
import calendar
import xlwt
from .forms import StudentLoginForm, AddStudentToProjectForm, StudentNewLogForm
from .models import Project, Student, Participation, Log

# 1-4, 5-7, 8, 9-12 (months of a year)
# PERIODS = [[0, 1, 2, 3], [4, 5, 6], [7], [8, 9, 10, 11]]
PERIODS = [[1, 2, 3, 4], [5, 6, 7], [8], [9, 10, 11, 12]]


class TimedeltaToHoursMixin(timedelta, object):
   def __str__(self):
        seconds = self.total_seconds()
        hours = seconds // 3600
        minutes = (seconds % 3600) // 60
        seconds = seconds % 60
        str = '{}h {}min'.format(int(hours), int(minutes))
        return (str)


# gets the context data for credits by period for CEOconclusion and CEOexcel:
class GetCreditsByPeriodMixin(object):

    def get_context_data(self, **kwargs):
        context = super(GetCreditsByPeriodMixin, self).get_context_data(**kwargs)
        # list of dictionaries with student info:
        context['stud_dict_list'] = []
        context['credits_list'] = []

        for s in context['student_list']:
            context['stud_dict'] = {}
            context['stud_dict']['name'] = s.first_name + " " + s.last_name
            context['stud_dict']['student_number'] = s.student_number
            context['stud_dict']['group'] = s.group

            today = datetime.today()
            for period in PERIODS:
                if today.date().month in period:
                    context['total_hours'] = s.log_set.all().filter(date__month__in=period).aggregate(Sum('hours'))

                    # COUNTING REMAINING HOURS:
                    prev_period = PERIODS[PERIODS.index(period) - 1]
                    context['prev_period_hours'] = s.log_set.all().filter(date__month__in=prev_period).aggregate(Sum('hours'))
                    if context['prev_period_hours']['hours__sum'] is None:
                        credit_remainder = 0
                    else:
                        credit_remainder = context['prev_period_hours']['hours__sum'].total_seconds() / 3600 / 27 % 1

            total_time = context['total_hours']['hours__sum']
            if total_time is None:
                context['stud_dict']['credits'] = credit_remainder
            if total_time is not None:
                context['stud_dict']['credits'] = (int(total_time.total_seconds() / 3600 / 27) + credit_remainder)//1
            context['credits_list'].append(context['stud_dict']['credits'])
            context['stud_dict_list'].append(context['stud_dict'])
        return context


# list of projects for a student:
class ProjectList(ListView):
    template_name = 'credits_score/project_list.html'

    def get_queryset(self):
        user = self.request.user
        participation_list = []
        for item in Participation.objects.filter(student_id=user.id):
            project_id = item.project_id
            participation_list.append(project_id)

        return Project.objects.filter(id__in=participation_list)

    def get_context_data(self, **kwargs):
        context = super(ProjectList, self).get_context_data(**kwargs)
        user = self.request.user
        context['all_hours'] = Log.objects.filter(student=user).filter(status='Approved').aggregate(Sum('hours'))
        context['general_hours_register'] = Project.objects.get(title='General Hours Register')
        if context['all_hours']['hours__sum'] is not None:
            context['total_time'] = context['all_hours']['hours__sum'].total_seconds()/3600
        else:
            context['all_hours']['hours__sum'] = 0

        return context


# Creates a new working hours log and reloads to the same page:
class StudentNewLog(CreateView):
    model = Log
    template_name = 'credits_score/project_details.html'
    form_class = StudentNewLogForm

    def get_context_data(self, **kwargs):
        context = super(StudentNewLog, self).get_context_data(**kwargs)
        context['project'] = Project.objects.get(id=self.kwargs['pk'])
        context['logs'] = Log.objects.filter(project=context['project']).filter(student=self.request.user)
        return context

    def get_initial(self):
        pk = self.kwargs['pk']
        return {'project': Project.objects.get(id=pk), 'student': self.request.user}

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        start = form.data['start_time']
        end = form.data['end_time']
        start_t = datetime.strptime(start, '%H:%M')
        end_t = datetime.strptime(end, '%H:%M')
        hours = end_t - start_t
        if form.is_valid():
            log = form.save(commit=False)
            log.hours = hours
            log.save()
            return self.form_valid(log)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('projects:detail', kwargs={'pk': self.kwargs['pk']})


# Creates a new instance of Participation
class AddStudentToProject(CreateView):
    model = Participation
    template_name = 'credits_score/participation_form.html'
    form_class = AddStudentToProjectForm

    def get_initial(self):
        slug = self.kwargs['slug']
        return {'project': Project.objects.get(title=slug).id}

    def get_form_kwargs(self):
        kwargs = super(AddStudentToProject, self).get_form_kwargs()
        # update the kwargs for the form init method
        kwargs.update(self.kwargs)  # self.kwargs contains all url conf params
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddStudentToProject, self).get_context_data(**kwargs)
        context['project'] = Project.objects.get(title=self.kwargs['slug'])
        context['participations'] = Participation.objects.filter(project=context['project'])
        return context

    def get_success_url(self):
        return reverse_lazy('projects:edit_project', kwargs={'slug': self.kwargs['slug']})


# deletes student from the project
class DeleteParticipation(DeleteView):
    model = Participation

    def get_success_url(self):
        return reverse_lazy('projects:edit_project', kwargs={'slug': self.kwargs['slug']})


# CEO can change Project info:
class UpdateProjectInfo(UpdateView):
    model = Project
    fields = ['title', 'description']
    template_name = 'credits_score/update_project.html'

    def get_success_url(self):
        return reverse_lazy('projects:edit_project', kwargs={'slug': self.request.POST['title']})


# Upon login redirect user to CEOstart or student page:
class StudentLoginFormView(FormView):
    form_class = StudentLoginForm  # form from forms.py
    template_name = 'credits_score/student_login.html'

    def post(self, request):
        form = self.form_class(request.POST)
        email = request.POST['email']
        password = request.POST['password']

        user = authenticate(email=email, password=password)
        # print user
        if user is not None:
            if user.is_active:
                if user.is_CEO or user.is_superuser:
                    login(request, user)
                    return redirect('CEOstart')
                else:
                    login(request, user)
                    return redirect('projects:project_list')
            else:
                return HttpResponse("Inactive user.")
        else:
            return render(request, self.template_name, {'form':form})


# mixin is added to restrict student access CEO's page
class CEOstart(UserPassesTestMixin, ListView):
    model = Project
    template_name = 'credits_score/ceostart.html'

    def get_context_data(self, **kwargs):
        context = super(CEOstart, self).get_context_data(**kwargs)
        context['project_list'] = Project.objects.all()  # list for projects + project names
        context['student_list'] = Student.objects.exclude(is_CEO=True).exclude(is_superuser=True).exclude(is_active=False)

        current_year = datetime.today().year
        context['students_added_this_year'] = Student.objects.exclude(is_CEO=True).exclude(is_superuser=True).filter(date_joined__year=current_year).count()
        context['projects_added_this_year'] = Project.objects.filter(start_date__year=current_year).count()
        context['hours_this_year'] = Log.objects.filter(date__year=current_year).aggregate(Sum('hours'))

        q = self.request.GET.get("q")
        if q:
            context['student_list'] = Student.objects.exclude(is_CEO=True).\
                exclude(is_superuser=True).\
                filter(first_name__contains=q)
        return context

    def handle_no_permission(self):
        return redirect('access-denied')

    def test_func(self):
        return self.request.user.is_CEO


# table of results for CEO:
class CEOconclusions(GetCreditsByPeriodMixin, ListView):
    model = Student
    template_name = 'credits_score/ceo_conclusions.html'
    queryset = Student.objects.filter(is_superuser=False).filter(is_CEO=False)


# if student tries to access CEO page:
class AccessDenied(TemplateView):
    template_name = 'credits_score/access_denied.html'


# CEO's view of students info and edit option
class CeoStudentInfo(TimedeltaToHoursMixin, DetailView):
    model = Student
    template_name = 'credits_score/ceo_student.html'

    def get_context_data(self, **kwargs):
        context = super(CeoStudentInfo, self).get_context_data(**kwargs)
        today = datetime.now()
        current_month = today.date().month
        current_year = today.date().year
        string_month = calendar.month_name[current_month]

        if Student.objects.get(id=self.kwargs['pk']).remainder is None:
            extra_credits = 0
        else:
            extra_credits = Student.objects.get(id=self.kwargs['pk']).remainder
        print extra_credits

        context['displayed_month'] = string_month
        context['int_month'] = current_month
        context['id'] = self.kwargs['pk']
        context['logs'] = Log.objects.filter(student=context['id']).filter(date__month=current_month).order_by('date')

        """ Check which one of 4 periods does the current date belongs to,
        then calculate total hours and credits only for the current period.
        (PERIODS is a list of lists) """
        for period in PERIODS:
            if today.date().month in period:
                prev_period = PERIODS[PERIODS.index(period) - 1]
                prev_prev_period = PERIODS[PERIODS.index(period) - 2]

                # convert to actual month numbers:
                # prev_period_month = []
                # for n in prev_period:
                #     n += 1
                #     prev_period_month.append(n)
                if prev_period == PERIODS[3]:
                    prev_period_year = current_year - 1
                else:
                    prev_period_year = current_year

                student = Student.objects.get(id=context['id'])
                last_checkout_month = student.last_checkout_month
                model_remainder = student.remainder

                if Log.objects.filter(student=context['id']).filter(status='Pending').filter(date__year=prev_period_year).filter(date__month__in=prev_period).count() < 1:
                    if last_checkout_month is None:
                        student.remainder = 0
                        student.last_checkout_month = current_month
                        student.save()

                    if last_checkout_month not in period:
                        if last_checkout_month not in prev_period:
                            prev_period += prev_prev_period
                        # COUNTING REMAINING HOURS:
                        context['prev_period_hours'] = Log.objects. \
                            filter(student=context['id']). \
                            filter(status='Approved'). \
                            filter(date__year=prev_period_year). \
                            filter(date__month__in=prev_period).aggregate(Sum('hours'))
                        # print context['prev_period_hours']

                        # COUNTING CREDITS REMAINDER:
                        if context['prev_period_hours']['hours__sum'] is None:
                            period_remainder = 0
                        else:
                            period_remainder = context['prev_period_hours']['hours__sum'].total_seconds() / 3600 / 27 % 1

                        period_remainder += model_remainder
                        if period_remainder >= 1:
                            extra_credits = period_remainder // 1
                            period_remainder = period_remainder % 1
                        else:
                            extra_credits = 0

                        student.remainder = period_remainder
                        student.last_checkout_month = current_month
                        student.save()

                context['total_hours'] = Log.objects.\
                    filter(student=context['id']).\
                    filter(status='Approved').\
                    filter(date__year=current_year).\
                    filter(date__month__in=period).aggregate(Sum('hours'))


        total_time = context['total_hours']['hours__sum']
        if total_time is not None:
            # ADDING REMINDER TO THE CURRENT SUM OF CREDITS:
            context['credits'] = total_time.total_seconds()/3600/27 + extra_credits
            context['total_hours']['hours__sum'] = TimedeltaToHoursMixin(seconds=context['total_hours']['hours__sum'].total_seconds()+extra_credits*27*3600)
        else:
            context['credits'] = extra_credits
            context['total_hours']['hours__sum'] = 0

        context['int_year'] = current_year
        # if either Previous or Next button clicked:
        if self.request.GET:
            # getting parameters from the template
            requested_month = int(self.request.GET['month'])
            requested_year = int(self.request.GET['year'])
            if requested_month >= 13:
                requested_month = 1
                requested_year += 1
            if requested_month <= 0:
                requested_month = 12
                requested_year -= 1
            context['logs'] = Log.objects.filter(student=context['id']).filter(date__month=requested_month, date__year=requested_year).order_by('date')
            context['int_month'] = requested_month
            # the month is displayed in letter format:
            context['displayed_month'] = calendar.month_name[requested_month]
            context['int_year'] = requested_year
        return context


class CreateProject(CreateView):
    model = Project
    fields = ['title', 'description', 'funding']
    template_name = 'credits_score/create_project.html'

    def get_success_url(self):
        return reverse_lazy('CEOstart')


class DeleteProject(DeleteView):
    model = Project

    def get_success_url(self):
        return reverse_lazy('CEOstart')


# CEO is able to register a new student:
class CreateUser(CreateView):
    model = Student
    fields = ['username', 'first_name', 'last_name', 'email', 'position', 'student_number', 'group']
    template_name = 'credits_score/create_student.html'

    # def get_form(self, request, obj=None, **kwargs):
    #     form = super(CreateUser, self).get_form(request, obj, **kwargs)
    #     form.fields['username', 'first_name', 'last_name', 'email'].widget = forms.TextInput(attrs={'required': 'true'})
    #     return form

    def get_success_url(self):
        return reverse_lazy('CEOstart')


# class UpdateUser(UpdateView):
#     model = Student
#     fields = ['username', 'first_name', 'last_name', 'email', 'student_number', 'group']
#     template_name = 'credits_score/update_student.html'
#
#     def get_success_url(self):
#         return reverse_lazy('CEOstart')


# Approve or reject log on CEOstudent page:
class CeoLogStatus(UpdateView):
    model = Log
    fields = ['status']

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(BaseUpdateView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        # display page with the last month after approving, not current month
        displ_month = self.get_object().date.month
        return reverse('projects:ceo_student', kwargs={'pk': self.get_object().student.id}) + "?month=" + str(
            displ_month) + "&year=" + str(self.get_object().date.year)


# Approve ALL logs of the student for the particular month:
class CeoLogStatusAll(View):

    def dispatch(self, request, *args, **kwargs):
        self.object = Log.objects.all().filter(student=kwargs['pk']).filter(date__year=kwargs['y']).filter(date__month=kwargs['m']).update(status='Approved')
        # HttpResponseRedirect accepts only URLs. Reverse() creates URL
        return HttpResponseRedirect(reverse('projects:ceo_student', kwargs={'pk': kwargs['pk']}) + "?month=" + str(
            kwargs['m']) + "&year=" + str(kwargs['y']))


# Excel-file generator
class CEOexcel(GetCreditsByPeriodMixin, ListView):
    model = Student
    Student.is_active = False
    queryset = Student.objects.filter(is_superuser=False).filter(is_CEO=False)

    # why render to response?
    # It's executed one of the last ones, therefore context data can get fetched
    def render_to_response(self, context, **response_kwargs):
        response = HttpResponse(content_type='application/ms-excel')
        filename = "theFirma_worksheet_" + str(datetime.today().date()) + ".xls"
        response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Students')

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['First name', 'Last name', 'Student number', 'Group', 'Credits', 'Grade', 'Teacher', 'Research & Development credits', 'Task']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        font_style = xlwt.XFStyle()

        # data query for table rows:
        rows = Student.objects.filter(is_CEO=False).\
            filter(is_superuser=False).\
            values_list('first_name', 'last_name', 'student_number', 'group')
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)
        # credits data, coming from the mixin:
        row_num = 0
        for row in self.get_context_data()['credits_list']:
            row_num += 1
            print row
            ws.write(row_num, 4, row)
        wb.save(response)
        return response


class DeactivateStudent(UpdateView):

    model = Student
    fields = ['is_active']

    def post(self, request, *args, **kwargs):
        # self.object = self.get_object()
        self.object = Student.objects.get(id=kwargs['pk'])
        return super(DeactivateStudent, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('CEOstart')

