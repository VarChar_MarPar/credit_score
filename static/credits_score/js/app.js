// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByClassName("project-status");
var i;
for (i = 0; i < myNodelist.length; i++) {
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    myNodelist[i].appendChild(span);
}
// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
        var div = this.parentElement;
        div.style.display = "none";
    }
}

// Create a new list item when clicking on the "Add" button
function newElement() {
    var date;
    var WCTs;
    date = document.getElementById('date').value;
    WCTs = document.getElementById('workingContents').value;
    var inputValue = "Date:" + " " + date + " " + "Working Contents:" + " " + WCTs;
    var li = document.createElement("li");
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (date == "" & WCTs == "") {
        alert("You must write something!");
    } else {
        document.getElementById("myUL").appendChild(li);
    }

    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    li.appendChild(span);

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function() {
            var div = this.parentElement;
            div.style.display = "none";
        }
    }
}

//Clear all input
function clearInput(){
    document.getElementById('date').value = "";
    document.getElementById('startWork').value = "";
    document.getElementById('endWork').value = "";
    document.getElementById('workingContents').value = "";
}





// // Click on a close button to hide the current list item
//     var close = document.getElementsByClassName("close");
//     var i;
//     for (i = 0; i < close.length; i++) {
//         close[i].onclick = function() {
//             var div = this.parentElement;
//             div.style.display = "none";
//         }
//     }
