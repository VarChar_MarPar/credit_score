from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.ProjectList.as_view(), name='project_list'),
    url(r'^$', views.ProjectList.as_view(), name='project_list'),
    url(r'^(?P<pk>[ a-zA-Z0-9_.-]*)/$', views.StudentNewLog.as_view(), name='detail'),
    url(r'^(?P<slug>[ a-zA-Z0-9_.-]*)/ceo$', views.AddStudentToProject.as_view(), name='edit_project'),
    url(r'^(?P<slug>[ a-zA-Z0-9_.-]*)/ceo/(?P<pk>[0-9]+)/delete$', views.DeleteParticipation.as_view(), name='delete_participation'),
    url(r'^ceo/(?P<pk>[0-9]+)/deactivate', views.DeactivateStudent.as_view(), name='ceo_deactivate'),
    url(r'^(?P<pk>[0-9]+)/ceo/update$', views.UpdateProjectInfo.as_view(), name='update_project'),
    url(r'^ceo/(?P<pk>[0-9]+)/', views.CeoStudentInfo.as_view(), name='ceo_student'),
    url(r'^ceo/conclusions/', views.CEOconclusions.as_view(), name='ceo_conclusions'),  # CONCLUSION TABLE
    url(r'^ceo/excel/', views.CEOexcel.as_view(), name='ceo_excel'),  # EXCEL GENERATOR
    url(r'^ceo/create/$', views.CreateProject.as_view(), name='add_project'),
    url(r'^ceo/createuser/$', views.CreateUser.as_view(), name='add_user'),
    url(r'^ceo/log/(?P<pk>[0-9]+)/update', views.CeoLogStatus.as_view(), name='ceo_log_status'),
    url(r'^ceo/log/(?P<pk>[0-9]+)/(?P<m>[0-9]+)/(?P<y>[0-9]+)/update', views.CeoLogStatusAll.as_view(), name='ceo_log_status_all'),
]
