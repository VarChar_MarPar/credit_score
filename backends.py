from django.contrib.auth.backends import ModelBackend
from .models import Student

# this file's functions change authenticate() function according to our needs.
# In this case it is possible to authenticate using email and password
# Also, User model has been changed to be Student (as we use Student for authentication now)


class EmailUsernameStudentBackend(ModelBackend):
    def authenticate(self, email=None, password=None, **kwargs):
        try:
            o = Student.objects.get(email=email, password=password)
        except Student.DoesNotExist:
            return None
        return Student.objects.get(email=o.email)

    def get_user(self, user_id):
        try:
            return Student.objects.get(pk=user_id)
        except Student.DoesNotExist:
            return None
